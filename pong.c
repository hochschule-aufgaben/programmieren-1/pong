#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <math.h>
#include "digits.h"
#include "speed.h"
#include "position.h"

#define PLAYER_WIDTH 6
#define PLAYER_HEIGHT 10

#define BALL_HEIGHT 3
#define BALL_WIDTH 4

#define BALL_X_START 20
#define BALL_Y_START 20
#define BALL_SPEED_X_START 5
#define BALL_SPEED_Y_START 0

#define PLAYER_STEP 5

#define ENEMY_REACT_THRESHOLD 30
#define ENEMY_SPEED_Y 1

#define min(a, b) a < b ? a : b
#define max(a, b) a > b ? a : b


// int screenBoundaryX = COLS;
// int screenBoundaryY = ROWS;

void drawBox(Position *boxPosition, int width, int height);
void drawBall(Position *ballPosition);
void drawPlayer(Position *);
void updatePlayerPositionOnKeys(Position *playerPosition, char *shouldKeepRunning, int *playerScore, int *enemyScore);
void calculateBallPosition(Speed *ballSpeed, Position *ballPosition);
char doesPlayerTouchBall(Position *playerPosition, Position *ballPosition);
char doesRobotTouchBall(Position *playerPosition, Position *ballPosition);
char didBallEnterPlayerHitzone(Position *ballPosition);
char didBallEnterRobotHitzone(Position *ballPosition);
void calculateBallPlayerBounce(Position *ballPosition, Position *playerPosition, Speed *ballSpeed);
void calculateBallEnemyBounce(Position *ballPosition, Position *playerPosition, Speed *ballSpeed);
int calculateEnemyHitY(Position *ballPosition, Speed *ballSpeed);
void enemyReact(Position *ballPosition, Speed *ballSpeed, Position *enemyPosition);
void drawScores(int playerScore, int enemyScore);

int main(){
	char run = 1;
	
	int ballBlinkCount = 0;

	int playerScore = 0, enemyScore = 0;


	initscr();
	timeout(1);
	curs_set(0);


	Position playerPosition = {
		0,
		LINES / 2
	};

	Position robotPosition = {
		COLS - PLAYER_WIDTH - 1,
		LINES / 2, 
	};
	
	Speed ballSpeed = {
		BALL_SPEED_X_START,
		BALL_SPEED_Y_START
	};
	Position ballPosition = {
		BALL_X_START,
		BALL_Y_START
	};

	while(run){
		if(ballBlinkCount == 0){
			updatePlayerPositionOnKeys(&playerPosition, &run, &playerScore, &enemyScore);
			calculateBallPosition(&ballSpeed, &ballPosition);
		}

		clear();

		if((ballPosition.x + BALL_WIDTH) >= COLS){
			ballSpeed.speedX = -1 * fabs(ballSpeed.speedX);
			ballPosition.x = COLS - BALL_WIDTH; 
		}
		if(ballPosition.y <= 0){
			ballSpeed.speedY = fabs(ballSpeed.speedY);
			ballPosition.y = 0;
		}
		if((ballPosition.y + BALL_HEIGHT) >= LINES){
			ballSpeed.speedY = -fabs(ballSpeed.speedY);
			ballPosition.y = LINES - BALL_HEIGHT; 
		}

		enemyReact(&ballPosition, &ballSpeed, &robotPosition);

		if(ballBlinkCount == 0 && didBallEnterPlayerHitzone(&ballPosition)){
			if(doesPlayerTouchBall(&playerPosition, &ballPosition)){
				calculateBallPlayerBounce(&ballPosition, &playerPosition, &ballSpeed);
			}else{
				enemyScore++;
				ballBlinkCount = 10;
			}
		}else if(ballBlinkCount == 0 && didBallEnterRobotHitzone(&ballPosition)){
			if(doesRobotTouchBall(&robotPosition, &ballPosition)){
				calculateBallEnemyBounce(&ballPosition, &robotPosition, &ballSpeed);
			}else{
				playerScore++;
				ballBlinkCount = 10;
			}
		}

		if(!(ballBlinkCount & 1)){
			drawBall(&ballPosition);
		}
		if(ballBlinkCount > 0){
			if(ballBlinkCount == 1){
				ballPosition.x = BALL_X_START;
				ballPosition.y = BALL_Y_START;

				ballSpeed.speedX = BALL_SPEED_X_START;
				ballSpeed.speedY = BALL_SPEED_Y_START;
				flushinp();
			}
			ballBlinkCount--;
		}
		drawPlayer(&playerPosition);
		drawPlayer(&robotPosition);
		drawScores(playerScore, enemyScore);
		refresh();
		napms(100);
	}
	endwin();

	return 0;
}

void drawScores(int playerScore, int enemyScore){
	const int distanceFromCenter = 8;
	for(int x = 0; x < BYTES_PER_DIGIT * 2; x++){
		char currentChar = digit_bitmap[playerScore * BYTES_PER_DIGIT + x / 2];
		for(int y = 0; y < 8; y++){
			move(2 + y, COLS / 2 - (BYTES_PER_DIGIT - x) - distanceFromCenter);
			if(currentChar & (1 << y)){
				printw("#");
			}
		}
	}

	move(3, COLS / 2 - 1);
	printw("##");
	move(4, COLS / 2 - 1);
	printw("##");

	move(7, COLS / 2 - 1);
	printw("##");
	move(8, COLS / 2 - 1);
	printw("##");

	for(int x = 0; x < BYTES_PER_DIGIT * 2; x++){
		char currentChar = digit_bitmap[enemyScore * BYTES_PER_DIGIT + x / 2];
		for(int y = 0; y < 8; y++){
			move(2 + y, COLS / 2 - (BYTES_PER_DIGIT - x) + distanceFromCenter);
			if(currentChar & (1 << y)){
				printw("#");
			}
		}
	}
}

void enemyReact(Position *ballPosition, Speed *ballSpeed, Position *enemyPosition){
	if(ballPosition->x < ENEMY_REACT_THRESHOLD) return;
	if(ballSpeed->speedX <= 0) return;
	int hitY = calculateEnemyHitY(ballPosition, ballSpeed);
	int difY = hitY - enemyPosition->y;
	if(abs(difY) < 2) return;

	enemyPosition->y += (difY > 0) ? ENEMY_SPEED_Y : (-ENEMY_SPEED_Y);
	enemyPosition->y = max(enemyPosition->y, 0);
	enemyPosition->y = min(enemyPosition->y, LINES - PLAYER_HEIGHT);
}

// calculates, where the ball is going to collide with the enemy hitbox
int calculateEnemyHitY(Position *ballPosition, Speed *ballSpeed){
	float difX = (COLS - PLAYER_WIDTH) - ballPosition->x;
	float timeTicksUntilHitX = difX / ballSpeed->speedX;
	float traveledYUntilHit = timeTicksUntilHitX * ballSpeed->speedY;
	float finalHitY = ballPosition->y + traveledYUntilHit;

	return abs((int) finalHitY) % LINES;
}

void calculateBallPlayerBounce(Position *ballPosition, Position *playerPosition, Speed *ballSpeed){
	float ballCenter = ballPosition->y + BALL_HEIGHT / 2;
	float playerCenter = playerPosition->y + PLAYER_HEIGHT / 2;

	float speedRandomY = (rand() / RAND_MAX);
    ballSpeed->speedY += (ballCenter - playerCenter) * 0.3 + speedRandomY;	
	ballSpeed->speedX = fabs(ballSpeed->speedX);
}

void calculateBallEnemyBounce(Position *ballPosition, Position *playerPosition, Speed *ballSpeed){
	float ballCenter = ballPosition->y + BALL_HEIGHT / 2;
	float playerCenter = playerPosition->y + PLAYER_HEIGHT / 2;

	float speedRandomY = (rand() / RAND_MAX);
    ballSpeed->speedY += (ballCenter - playerCenter) * 0.3 + speedRandomY;	
	ballSpeed->speedX = -fabs(ballSpeed->speedX);
}

char didBallEnterPlayerHitzone(Position *ballPosition){
	return ballPosition->x <= PLAYER_WIDTH;
}

char didBallEnterRobotHitzone(Position *ballPosition){
	return (ballPosition->x + BALL_WIDTH) >= (COLS - PLAYER_WIDTH);
}

char doesRobotTouchBall(Position *playerPosition, Position *ballPosition){
	return (ballPosition->y + BALL_HEIGHT) >= playerPosition->y && ballPosition->y <= (playerPosition->y + PLAYER_HEIGHT);
}

char doesPlayerTouchBall(Position *playerPosition, Position *ballPosition){
	return (ballPosition->y + BALL_HEIGHT) >= playerPosition->y && ballPosition->y <= (playerPosition->y + PLAYER_HEIGHT);
}

void calculateBallPosition(Speed *ballSpeed, Position *ballPosition){
	ballPosition->x += ballSpeed->speedX;
	ballPosition->y += ballSpeed->speedY;
}

void updatePlayerPositionOnKeys(Position *playerPosition, char *shouldKeepRunning, int *playerScore, int *enemyScore){
	char key = getch();
	if(key == 'q'){
		*shouldKeepRunning = 0;
	}else if(key != ERR){
		if(key == 'w'){
			playerPosition->y -= PLAYER_STEP;
			if(playerPosition->y < 0) playerPosition->y = 0;
		}else if(key == 's'){
			playerPosition->y += PLAYER_STEP;
			if(playerPosition->y > (LINES - PLAYER_HEIGHT)) playerPosition->y = LINES - PLAYER_HEIGHT;
		}else if(key == 'p'){
			(*playerScore)++;
		}else if(key == 'e'){
			(*enemyScore)++;
		}
	}
}

void drawPlayer(Position *playerPosition){
	drawBox(playerPosition, PLAYER_WIDTH, PLAYER_HEIGHT);
}

void drawBall(Position *ballPosition){
	drawBox(ballPosition, BALL_WIDTH, BALL_HEIGHT);
}

// draws a box with upper left corner at positionX, positionY
void drawBox(Position *position, int width, int height){
	for(int y = 0; y < height; y++){
		move(position->y + y, position->x);
		for(int x = 0; x < width; x++){
			addch('#');
		};
	}
}
